//
//  ViewController.m
//  HZWCategories
//
//  Created by ZhangMing on 4/1/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import "ViewController.h"
#import "NSArray+HZW.h"
#import "People.h"
#import "UINavigationBar+HZW.h"
#import "UITabBar+HZW.h"
#import "UIImage+HZW.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    People *p1 = [[People alloc] init];
    p1.peopleId = @"1";
    
    People *p2 = [[People alloc] init];
    p2.peopleId = @"2";
    
    People *p3 = [[People alloc] init];
    p3.peopleId = @"3";
    
    People *p4 = [[People alloc] init];
    p4.peopleId = @"4";
    
    NSArray *list1 = @[p1, p2];
    
    People *p11 = [[People alloc] init];
    p11.peopleId = @"1";
    
    People *p22 = [[People alloc] init];
    p22.peopleId = @"2";
    
    People *p33 = [[People alloc] init];
    p33.peopleId = @"3";
    
    People *p44 = [[People alloc] init];
    p44.peopleId = @"4";
    
    NSArray *list2 = @[p11, p22, p33, p44];
    
    NSLog(@"%@", [list1 distinctObjectsFromArray:list2]);
    
    
    NSArray *list3 = @[p1, p11, p22];
    NSLog(@"list3 = %@", [list3 distinctObjects]);
    
    [self.navigationController.navigationBar hideBottomHairline];
    
//    self.view.backgroundColor = [UIColor blackColor];
    
    UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 200, self.view.bounds.size.width, 40)];
    view.image = [UIImage generateImageWithCGSize:CGSizeMake(1, 1) color:[UIColor redColor]];
//    [self.view addSubview:view];
    
//    [[UITabBar appearance] setShadowImage:[UIImage generateImageWithCGSize:CGSizeMake(1, 1) color:[UIColor whiteColor]]];
    UIImage *image = [UIImage generateImageWithCGSize:CGSizeMake(1, 1) color:[UIColor whiteColor]];
//    [[UITabBar appearance] setShadowImage:image];
//    [[UITabBar appearance] setBackgroundImage:[UIImage new]];
    
//    [self.tabBarController.tabBar setValue:@(NO) forKey:@"_hidesShadow"];
    
    
    [self.tabBarController.tabBar setShadowImage:image];
    [self.tabBarController.tabBar setBackgroundColor:[UIColor redColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
