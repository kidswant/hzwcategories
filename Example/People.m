//
//  People.m
//  HZWCategories
//
//  Created by ZhangMing on 4/15/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import "People.h"

@implementation People

- (BOOL)isEqual:(id)object
{
    if (object == nil) {
        return NO;
    }
    
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    People *comparedObject = (People *)object;
    if ([comparedObject.peopleId isEqualToString:self.peopleId]) {
        return YES;
    }
    
    return NO;
}

- (NSString *)description
{
    return self.peopleId;
}
@end
