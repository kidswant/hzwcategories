//
//  People.h
//  HZWCategories
//
//  Created by ZhangMing on 4/15/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface People : NSObject

@property (nonatomic, copy) NSString *peopleId;

@end
