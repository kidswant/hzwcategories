//
//  NSArray+HZW.h
//  HZWCategories
//
//  Created by ZhangMing on 4/15/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (HZW)

- (NSArray *)distinctObjects;

- (NSArray *)distinctObjectsFromArray:(NSArray *)array;

@end
