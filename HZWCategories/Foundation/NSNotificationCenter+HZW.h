//
//  NSNotificationCenter+Kidswant.h
//  KidswantKit
//
//  Created by Kidswant on 2/25/16.
//  Copyright © 2016 Kidswant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNotificationCenter (HZW)

- (void)postNotificationOnMainThread:(NSNotification *)notification;

- (void)postNotificationOnMainThread:(NSNotification *)notification
                       waitUntilDone:(BOOL)wait;

- (void)postNotificationOnMainThreadWithName:(NSString *)name
                                      object:(id)object;

- (void)postNotificationOnMainThreadWithName:(NSString *)name
                                      object:(id)object
                                    userInfo:(NSDictionary *)userInfo;

- (void)postNotificationOnMainThreadWithName:(NSString *)name
                                      object:(id)object
                                    userInfo:(NSDictionary *)userInfo
                               waitUntilDone:(BOOL)wait;

@end
