//
//  UIApplication+Kidswant.h
//  KidswantKit
//
//  Created by Kidswant on 2/25/16.
//  Copyright © 2016 Kidswant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (HZW)

@property (nonatomic, readonly) NSURL *documentsURL;
@property (nonatomic, readonly) NSString *documentsPath;

@property (nonatomic, readonly) NSURL *cachesURL;
@property (nonatomic, readonly) NSString *cachesPath;

@property (nonatomic, readonly) NSURL *libraryURL;
@property (nonatomic, readonly) NSString *libraryPath;

@property (nonatomic, readonly) NSString *appBundleName;

@property (nonatomic, readonly) NSString *appBundleID;

@property (nonatomic, readonly) NSString *appVersion;

@property (nonatomic, readonly) NSString *appBuildVersion;

@property (nonatomic, readonly) int64_t memoryUsage;

@property (nonatomic, readonly) float cpuUsage;

@end
