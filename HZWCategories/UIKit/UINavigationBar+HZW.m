//
//  UINavigationBar+HZW.m
//  HZWCategories
//
//  Created by ZhangMing on 4/21/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import "UINavigationBar+HZW.h"

@implementation UINavigationBar (HZW)

- (void)hideBottomHairline
{
    UIImageView *bottomHairlineView = [self hairlineImageViewInNavigationBar:self];
    bottomHairlineView.hidden = YES;
}

- (void)showBottomHariline
{
    UIImageView *bottomHairlineView = [self hairlineImageViewInNavigationBar:self];
    bottomHairlineView.hidden = NO;
}

- (UIImageView *)hairlineImageViewInNavigationBar:(UIView *)view
{
    if (view && [view isKindOfClass:[UIImageView class]] && (view).bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    
    NSArray *subviews = view.subviews;

    for (id view in subviews) {
        UIImageView *iv = [self hairlineImageViewInNavigationBar:view];
        if (iv) {
            return iv;
        }
    }
    
    return nil;
}

@end
