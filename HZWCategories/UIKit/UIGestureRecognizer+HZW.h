//
//  UIGestureRecognizer+Kidswant.h
//  KidswantKit
//
//  Created by ZhangMing on 3/15/16.
//  Copyright © 2016 Aaron Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIGestureRecognizer (HZW)

- (instancetype)initWithActionBlock:(void (^)(id sender))block;

- (void)addActionBlock:(void (^)(id sender))block;

- (void)removeAllActionBlock;

@end
