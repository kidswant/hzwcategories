//
//  UIView+UIColor_HZW.m
//  HZWCategories
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import "UIColor+HZW.h"

@implementation UIColor (HZW)

- (UIColor *)mixColorWithDestinationColor:(UIColor *)toColor percent:(CGFloat)percent
{
    CGFloat fromRed, fromGreen, fromBlue, fromAlpha;
    [self getRed:&fromRed green:&fromGreen blue:&fromBlue alpha:&fromAlpha];
    
    CGFloat toRed, toGreen, toBlue, toAlpha;
    [toColor getRed:&toRed green:&toGreen blue:&toBlue alpha:&toAlpha];
    UIColor *result = [UIColor colorWithRed:fromRed * percent + toRed * (1.0 - percent)
                                      green:fromGreen * percent + toGreen * (1.0 - percent)
                                       blue:fromBlue * percent + toBlue * (1.0 - percent) alpha:1.0f];
    return result;
}

@end
